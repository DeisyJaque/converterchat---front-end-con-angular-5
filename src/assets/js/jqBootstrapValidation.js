
  <!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ConverterChat</title>

    <!-- Bootstrap core CSS -->
    <link href="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
    
   

    <!-- Custom fonts for this template -->
    <link href="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- Custom styles for this template -->
    <link href="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/css/manychat.css" rel="stylesheet">
     

  </head>

  <body id="page-top">
          
    <!-- Navigation -->
    <nav class="navbar navbar navbar-expand-lg navbar-light " id="mainNav">
      <div class="container">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" id="toogler-click" value="true">
          
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="https://manychat-proyectokamila.c9users.io/precios/" id="precios">Precios</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contactos">Contactos</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#blog">Blog</a>
            </li>
		      	<li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="https://manychat-proyectokamila.c9users.io:8081/singin">Iniciar Sesión</a>
            </li>
            <li class="nav-item boton-nav" id="boton">
              <a class="mybutton-azul glow-button-azul" href="https://manychat-proyectokamila.c9users.io:8081/singin">Comenzar gratis</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
    <div class="row linea" >
              <div class="col-lg-12 linea-nav">
                  <h6></h6>
              </div>
      </div>    <!-- Section 1-->
            <section id="portfolio" class="mysection1" style="background: url(https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/header-bg-min.png);background-size:cover;background-repeat:no-repeat;background-position:center;background-attachment:fixed;"> 
        <div class="container">
          <div class="container">
                         <h2 class="text-center">Meet Messenger Marketing</h2>
                                   <h5 class="text-center">Te permite crear un bot de Facebook Messenger <br> para marketing, ventas y soporte. Es fácil y gratis.</h5>
                       
          	
          </div>  
           <div class="col-lg-8 mx-auto text-center">
                <a href="https://manychat-proyectokamila.c9users.io:8081/singin" class="mybutton glow-button">
                                        Iniciar Sesión                                   </a>
              </div>
              <div class="row">
                  <div class="col-lg-8 mx-auto text-center biglg">
                      <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/iphone.png" class="tlf1"  alt="Responsive image">
                  </div>
               <div class="col-lg-4 mx-auto text-center texto1">
                                              <h3 class="text-center color-text">¿Qué es un bot?</h3>
                                             
                                                <h6 class="text-sm-left color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                             
                       <div class="col-lg-8 mx-auto button-center">
                        	<a href="" class="mybutton-azul glow-button-azul"> 
                        	                             Conversar                                                   	</a><br>
                        	 <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/letrapeque.png" class="msj" alt="Responsive image" >
                      </div>
                        
                        
              </div>
               <div class="col-lg-8 mx-auto oculto" >
                      <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/iphone.png" class="img-responsive tlf1"  alt="Responsive image">
              </div>
              
             
          </div>
     
    </div>
      <div class="row">
              <div class="col-lg-12 linea-section">
                  <h6></h6>
              </div>
      </div>
    </section>

    <section id="portfolio" class="mysection">
      <div class="container">
        <div class="row">
          
           <div class="col-lg-4 mx-auto texto2">
                                  <p class="text-sm-left fuente2 color-text">Transmisiones</p>
                                               <h3 class="text-sm-left color-text">Enviar mensajes a sus subscriptores</h3>
                                                <h6 class="text-sm-left color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur  ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                               
           </div>
           <div class="col-lg-5 mx-auto text-sm-left">
             <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/2_iphones.png" class="img-responsive-alt" alt="Responsive image">
             
           </div>
        </div>

      </div>
    </section>
     <!--Sección 3-->
	    <section id="portfolio" style="background: url(https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/bg_chateando-min.png);background-repeat: no-repeat;background-position: center;background-size: cover;background-attachment: fixed;color:#f9f4f4;">
        <div class="container">
          <div class="row">
            <div class="col-lg-5 mx-auto texto2 biglg">
              <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/iphone2.png" class="img-responsive" alt="Responsive image">
            </div>
            <div class="col-lg-4 mx-auto texto3">
                                <p class="text-sm-left fuente">Chat en vivo</p>
                                                 <h3 class="text-sm-left ">Manténgase personal con Live Chat</h3>
                                                 <h6 class="text-sm-left ">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascet ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                    
             
            </div>
             <div class="col-lg-5 mx-auto tlf2 oculto">
              <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/iphone2.png" class="tlf3 img-responsive" alt="Responsive image">
            </div>
          </div>
        </div>
    </section>
    <!--Sección 4-->
	    <section id="portfolio">
        <div class="container">
          <div class="row">
              <div class="col-lg-5 mx-auto">
                <img src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/images/logo_mns2.png"  class="img-responsive" alt="Responsive image">
              </div>
              <div class="col-lg-4 mx-auto texto3">
                                          <h3 class="text-sm-left color-text">¿Por qué Facebook Messenger?</h3>
                                                            <h6 class="text-sm-left color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                  
              </div>
            </div>
        </div>
         <div class="row">
                <div class="col-lg-12 linea-section2">
                    <h6></h6>
                </div>
        </div>
      
    </section>
    <!--Sección 5 -- slide -->
	    <section id="portfolio" class="mysection5" >
        <div class="container">
           <div class="row">

                    <!-- Section Header -->
                    <div class="col-lg-12 mx-auto text-center">
                                          <h3 class="color-text text-center">Testimonios</h3>
                                           
                       
                      
                    </div>

                      <!-- teams Slider -->
                                         <div id="carouselExampleControls" class="carousel slide big" data-ride="carousel">
                      <div class="carousel-inner" role="listbox">
                        <div class="carousel-item active">
                         <div class="row">
                                                                            		                                                                  <div class="col-lg-4 mx-auto elemcont">
                                                                          			<h5 class="text-sm-left color-text">Deisy Jaque</h5>
                                  			                                  			                                  			<h5 class="text-sm-left azul">Cargo</h5>
                                  			                                        
                                         
                                           <img class="circle img-responsive text-center" src="https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/contacto.jpg"  />
                                         
                                                                           			<h6 class="text-sm-left elemcont color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                  			                                        
                                        </div> 
                               
                                    </div> 
                                 </div> 
                                                                   <div class="carousel-item">
                                      <div class="row">
                                           <div class="col-lg-4 mx-auto elemcont">
                                                                              			<h5 class="text-sm-left color-text">Deisy Jaque</h5>
                                  			                                  			                                  			<h5 class="text-sm-left azul">Cargo</h5>
                                  			                                        
                                         
                                           <img class="circle img-responsive text-center" src="https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/contacto.jpg"  />
                                             
                                                                               			<h6 class="text-sm-left elemcont color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                  			                                        
                                            </div> 
                                       
                                      </div> 
                                  </div> 
                               
                                
                               
                                            		                                                              <div class="carousel-item">
                                      <div class="row">
                                           <div class="col-lg-4 mx-auto elemcont">
                                                                              			<h5 class="text-sm-left color-text">Pedro Perez</h5>
                                  			                                  			                                  			<h5 class="text-sm-left azul">Cargo</h5>
                                  			                                        
                                         
                                           <img class="circle img-responsive text-center" src="https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/contacto.jpg"  />
                                             
                                                                               			<h6 class="text-sm-left elemcont color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                  			                                        
                                            </div> 
                                       
                                      </div> 
                                  </div> 
                               
                                
                               
                                            		                                                              <div class="carousel-item">
                                      <div class="row">
                                           <div class="col-lg-4 mx-auto elemcont">
                                                                              			<h5 class="text-sm-left color-text">José López</h5>
                                  			                                  			                                  			<h5 class="text-sm-left azul">Cargo</h5>
                                  			                                        
                                         
                                           <img class="circle img-responsive text-center" src="https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/contacto.jpg"  />
                                             
                                                                               			<h6 class="text-sm-left elemcont color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                  			                                        
                                            </div> 
                                       
                                      </div> 
                                  </div> 
                               
                                
                               
                                            		                                                              <div class="carousel-item">
                                      <div class="row">
                                           <div class="col-lg-4 mx-auto elemcont">
                                                                              			<h5 class="text-sm-left color-text">Laura Arce</h5>
                                  			                                  			                                  			<h5 class="text-sm-left azul">Cargo</h5>
                                  			                                        
                                         
                                           <img class="circle img-responsive text-center" src="https://manychat-proyectokamila.c9users.io/wp-content/uploads/2017/10/contacto.jpg"  />
                                             
                                                                               			<h6 class="text-sm-left elemcont color-text">Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim.</h6>
                                  			                                        
                                            </div> 
                                       
                                      </div> 
                                  </div> 
                               
                                
                               
                                          
                    
                    
                        
                          
                       
                        
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev" >
                        <span class="carousel-control-prev-icon" aria-hidden="true" ></span>
                        <span class="sr-only" >Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next" >
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                      </a>
                    </div>

                        <!-- Fin Slides -->

                    
                </div>
          </div>
        
    </section>
    
    <!-- Sección 6-->
	    <section id="portfolio" class="contacto">
      <div class="container">
           <div class="row">
            
            <div class="col-lg-12 mx-auto text-center">
                                    <h3 class="text-center texto3">Tu bot personalizado para tu marca o emprendimiento</h3>
                                 
              <a href="" class="btn btn-lg btn-outline">
                
                                      Contáctanos                                 
              </a>
            </div>
            </div>
      </div>
    </section>
    
    <!--Seción 7-->
	    <section id="portfolio" class="mysection7">
        <div class="container">
          <div class="row">
             <div class="col-lg-12 mx-auto text-center texto-espacio">
                                      <h3 class="text-center color-text">Preguntas comunes</h3>
                              </div>
                                         	            		                     <div class="col-lg-5 mx-auto text-sm-left">
                          <h5 class="text-center color-text">Pregunta 1</h5>
                          <h6 class="text-sm-left color-text"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetu ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </h6>
                        
                      </div>
                   
                                  
              
            	            		                       
                       <div class="col-lg-5 mx-auto text-sm-left">
                          <h5 class="text-center color-text">  Pregunta 2</h5>
                          <h6 class="text-sm-left color-text"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetu ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. </h6>
                        
                      </div>
                                   
              
            	            
            
            
                       
          </div>
        </div>
      </section>
    
	   
    
   <!-- Footer --> 
    <footer >
     
      <div class="footer-below">
        
        <div class="container">
          <div class="row ">
            
            <div class="col-lg-12 mx-auto text-center">
                <h5 class="text-center footer-below1">Messenger Marketing espera por ti<br> Comience hoy</h5>
                
                <a href="#" class="mybutton glow-button azul">
                  
                  Comience gratis
                  </a>
                  
                  <p class="footer-below2">2017</p>
                
              </div>
            </div>
              
           
          </div>
      </div>
      <div class="footer-above" >
        <div class="container">
          <div class="row ">
             <div class="col-lg-5 mx-auto text-center" ></div>
            <div class="col-lg-6 mx-auto text-sm-left footer-display">
               <a class="nav-link js-scroll-trigger" href="#"><h6>Precios</h6></a>
               <a class="nav-link js-scroll-trigger" href="#"><h6>Contactos</h6></a>
               <a class="nav-link js-scroll-trigger" href="#"><h6>Blog</h6></a>
               <a class="nav-link js-scroll-trigger" href="https://manychat-proyectokamila.c9users.io:8081/singin"><h6>Iniciar Sesión</h6></a>
              </div>
            </div>
              
           
          </div>
      </div>
                
      
         
    </footer>
     <!-- Bootstrap core JavaScript -->
    <script src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/vendor/jquery/jquery.min.js"></script>
    <script src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- Contact Form JavaScript -->
    <script src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/js/jqBootstrapValidation.js"></script>
   

    <!-- Custom scripts for this template -->
    <script src="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/js/manychat.min.js"></script>
  
     <script>
      $(document).ready(function(){
        $('#toogler-click').click(function(){
        if ( $(this).val() === "true" ) {
            $(this).val("false");
            
           $('#mainNav').css('background', '#00b1f3');
        } else if ($(this).val() === "false" ) {
            $(this).val("true");
          $('#mainNav').css('background', 'none');
        }
       
        });
        
      });
      
    </script>
    <script>
         var url="https://manychat-proyectokamila.c9users.io/wp-content/themes/Landing/js/jqBootstrapValidation.js";
        getClass = function(name_class){
        /*Buscar en todos los elementos (selector *)*/
       
        var cl = document.getElementsByTagName("*");
        /*Bucle para recorrer todos los elementos en buscar de la clase*/
        for(i = 0; i < cl.length; i++) {
        /*Si la clase es encontrada modificar el tamaño de fuente*/
        if (cl[i].className == name_class) {
            
            if(url=='https://manychat-proyectokamila.c9users.io/precios/'){
                cl[i].style.color = 'rgba(0,0,0,.5)';
                
            }
        }
        }
        }
        /*Hay que utilizar window.onload para que funcione*/
        window.onload = function(){
        getClass("nav-link js-scroll-trigger");
        }
        
         $(document).ready(function(){
        if(url=='https://manychat-proyectokamila.c9users.io/precios/'){
          $('#boton').css('display', 'block');
             $('#boton >a').css('padding','5px');
              $('#boton >a').css('font-weight', 'bold');
              $('#precios').css('color','#00b1f3');
              $('.linea').css('display','block');
              $('#toogler-click').css('color','rgba(0, 0, 0, 0.5)');
              $('#toogler-click').css('border-color','rgba(0, 0, 0, 0.5)');
}
      
        
      });
        
</script>
  </body>

</html>
 