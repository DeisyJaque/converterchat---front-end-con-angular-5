import { Injectable } from '@angular/core';
import { BaseLoginProvider } from './entities/base-login-provider';
import { SocialUser, LoginProviderClass } from './entities/user';

declare let FB: any;

@Injectable()
export class ApiService {


	public static readonly PROVIDER_ID = 'facebook';
	public loginProviderObj: LoginProviderClass = new LoginProviderClass();

	constructor() {
		this.loginProviderObj.id = "1941120542772303"; // Identificador de pruebas
		// this.loginProviderObj.id = "811383009070622"; // Identificador de chatboot
		this.loginProviderObj.name = 'facebook';
		this.loginProviderObj.url = 'https://connect.facebook.net/en_US/sdk.js';
	}

	initialize(): Promise<SocialUser> {
		console.log('initialize FB api Angular');
		return new Promise((resolve, reject) => {
			this.loadScript(this.loginProviderObj, () => {
				FB.init({
					appId: this.loginProviderObj.id,
					autoLogAppEvents: true,
					cookie: true,
					xfbml: true,
					version: 'v2.10'
				});
				FB.AppEvents.logPageView();

				FB.getLoginStatus(function (response: any) {
					if (response.status === 'connected') {
						const accessToken = FB.getAuthResponse()['accessToken'];
						FB.api('/me?fields=name,email,picture', (res: any) => {
							resolve(this.drawUser(Object.assign({}, {token: accessToken}, res)));
						});
					}
				});
			});
		});
	}

	static drawUser(response: any): SocialUser {
		console.log('**drawUser()**');
		let user: SocialUser = new SocialUser();
		user.id = response.id;
		user.name = response.name;
		user.email = response.email;
		user.token = response.token;
		user.image = 'https://graph.facebook.com/' + response.id + '/picture?type=normal';
		localStorage.setItem('user', JSON.stringify(user));
		return user;
	}

	signIn(): Promise<SocialUser> {
		return new Promise((resolve, reject) => {
			FB.login((response: any) => {
				if (response.authResponse) {
					const accessToken = FB.getAuthResponse()['accessToken'];
					FB.api('/me?fields=name,email,picture', (res: any) => {
						resolve(ApiService.drawUser(Object.assign({}, {token: accessToken}, res)));
					});
				}
			},{ scope: 'email,public_profile,manage_pages,publish_pages,pages_messaging,pages_show_list' });
		});
	}

	isLogin(): Promise<boolean>{
		return new Promise((resolve, reject) => {
			FB.getLoginStatus(function(response) {
				if (response.status === 'connected') {
					return true;
				}
				return false;
			});
		});
	}

	signOut(): Promise<any> {
		console.log("singOut api");
		localStorage.removeItem('user');
		return new Promise((resolve, reject) => {
			FB.logout((response: any) => {
				resolve();
			});
		});
	}

	pagePicture(): Promise<any>{

		console.log('pagePicture()');

		let page = JSON.parse(sessionStorage.getItem('selectedpage'));
		return new Promise((resolve, reject) => {
			FB.api(
				"/" + page.id,
				'GET',
				{
					"fields":"about,fan_count,link,picture",
					"access_token": page.access_token
				},
				function(response) {
					console.log('response pagePicture():',response);
					if (response && !response.error) {
						return response;
					}
					return reject(response.error);
			});
		});
	}

	// getPages(): Promise<any> {
	// 	if(FB != null){
	// 		FB.getLoginStatus(function (response: any) {
	// 			if (response.status === 'connected') {
	// 				const accessToken = FB.getAuthResponse()['accessToken'];
	// 				FB.api('me/accounts', (res: any) => {
	// 					console.log(res);
	// 					return res;
	// 				});
	// 			}
	// 		});
	// 	}else{
	// 		console.log("############### asd ##################");
	// 		return null;
	// 	}
	// }

	loadScript(obj: LoginProviderClass, onload: any): void {
		if (document.getElementById(obj.name)) { return; }
		let signInJS = document.createElement('script');
		signInJS.async = true;
		signInJS.src = obj.url;
		signInJS.onload = onload;
		if (obj.name === 'LINKEDIN') {
			signInJS.async = false;
			signInJS.text = ('api_key: ' + obj.id).replace('\'', '');
		}
		document.head.appendChild(signInJS);
	}

}
