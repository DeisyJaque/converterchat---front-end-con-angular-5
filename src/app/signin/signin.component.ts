import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService, FacebookLoginProvider } from 'angular5-social-login';
import { ApiService } from '../facebook/api.service';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Component({
	selector: 'app-signin',
	templateUrl: './signin.component.html',
	styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

	constructor( private socialAuthService: AuthService, private router: Router, private social: ApiService, private http: HttpClient) {

	}

	ngOnInit() {

	}

	public test(){
		this.social.signIn().then(
			(userData) => {

				console.log(" sign in data : " , userData);
				// Now sign-in with userData

				this.http.post('/api/login/', userData)
					.subscribe(res => {
						console.log('******************************************');
						console.log(res);

						userData.token = res['access_token'];
						localStorage.setItem('user', JSON.stringify(userData));

						if(res['created']){
							this.router.navigate(['singinlist']);
						}else{
							this.router.navigate(['dashboard']);
						}

					}, (err) => {
						console.log('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
						console.log(err);
					});
			}
		);
	}

	public socialSignIn(socialPlatform : string) {
		let socialPlatformProvider;

		socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;

		this.socialAuthService.signIn(socialPlatformProvider).then(
			(userData) => {

				localStorage.setItem('user', JSON.stringify(userData));
				console.log(socialPlatform+" sign in data : " , userData);
				// Now sign-in with userData
				this.router.navigate(['singinlist']);
			}
		);
	}

}
