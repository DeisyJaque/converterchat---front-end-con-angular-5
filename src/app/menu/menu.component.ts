import { Component, OnInit } from '@angular/core';
import {ViewChild, ElementRef, AfterViewInit} from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
//import * as $ from 'jquery';
// declare var $:JQueryStatic;
// Declaramos las variables para jQuery
@Component({
	selector: 'app-menu',
	templateUrl: './menu.component.html',
	styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

	public pages = [];
	public user : any;

	constructor(private router: Router, private http: HttpClient) { }

	ngOnInit() {

		this.user = JSON.parse(localStorage.getItem('user'));

		this.http.get('/api/mypages/'+this.user['id'])
		.toPromise()
		.then(
			response => {
				console.log('get',response);

				if(response['Count']){
					this.pages = response['Items'];
					localStorage.setItem('pages', JSON.stringify(this.pages));

					if(!sessionStorage.getItem('selectedpage')){
						sessionStorage.setItem('selectedpage', JSON.stringify(this.pages[0]));
					}

				}else{
					this.router.navigate(['singinlist']);
				}
			},
			error => {
				console.error(`Error: ${error.status} ${error.statusText}`);
				localStorage.removeItem('user');
				this.router.navigate(['singin']);
		});
	}

	changePage(p){
		console.log(p);
		sessionStorage.setItem('selectedpage', JSON.stringify(p));
		this.router.navigate(['dashboard',p.id]);

	}
}
