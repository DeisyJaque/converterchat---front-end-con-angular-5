import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { AuthService, FacebookLoginProvider } from 'angular5-social-login';
import { ApiService } from '../facebook/api.service';
import { SocialUser, LoginProviderClass } from '../facebook/entities/user';
import 'rxjs/add/operator/toPromise';

@Component({
	selector: 'app-dashboard',
	templateUrl: './dashboard.component.html',
	styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	public pages = {};
	public selectedpage : any;
	public pageInfo = {};

	constructor( private socialAuthService: AuthService, private router: Router, private http: HttpClient, private social: ApiService) { }

	ngOnInit() {

		console.log("********sessionStorage*************");
		console.log(sessionStorage.getItem('selectedpage'));
		console.log("********sessionStorage*************");

		if(sessionStorage.getItem('selectedpage') == undefined){

			console.log("********sessionStorage******undefined*******");

			let userId = JSON.parse(localStorage.getItem('user'))['id'];

			this.http.post('/api/pageinfo/',{selectpage: true , id: userId})
			.toPromise()
			.then(
				response => {
					console.log('/api/pageinfo/',response);
					this.pageInfo = response;
					this.pageInfo['picture'] = this.pageInfo['picture']['data']['url'];
				},
				error => {
					console.error(`Error: ${error.status} ${error.statusText}`);
					localStorage.removeItem('user');
					this.router.navigate(['singin']);
			});
		}else{

			this.selectedpage = JSON.parse(sessionStorage.getItem('selectedpage'));
			console.log('this.selectedpage',this.selectedpage);

			this.http.post('/api/pageinfo/',this.selectedpage)
			.toPromise()
			.then(
				response => {
					console.log('/api/pageinfo/',response);
					this.pageInfo = response;
					this.pageInfo['picture'] = this.pageInfo['picture']['data']['url'];
				},
				error => {
					console.error(`Error: ${error.status} ${error.statusText}`);
					localStorage.removeItem('user');
					this.router.navigate(['singin']);
			});
		}
	};

	// signOut(){
	// 	console.log('#############################');
	// 	this.social.initialize().then(function(){
	// 		this.social.signOut().then(function(){
	// 			alert("singOut component");
	// 			this.router.navigate(['singin']);
	// 		});
	// 	});
	// }

	signOut(){
		console.log('#############################');
		localStorage.removeItem('user');
		alert("singOut component");
		this.router.navigate(['singin']);
	}
	  public barChartOptions:any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels:string[] = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
  public barChartType:string = 'bar';
  public barChartLegend:boolean = true;

  public barChartData:any[] = [
    {data: [65, 59, 80, 81, 56, 55, 40], label: 'Series A'},
    {data: [28, 48, 40, 19, 86, 27, 90], label: 'Series B'}
  ];

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

}