import { Pipe, PipeTransform } from '@angular/core';

import { Audiencia } from './filter';

@Pipe({
    name: 'audiencia', pure: false})
    

export class FilterPipe implements PipeTransform {
  transform(items: Audiencia[], filter: Audiencia): Audiencia[] {
    if (!items || !filter) {
      return items;
    }
    // filter items array, items which match and return true will be kept, false will be filtered out
    return items.filter((item: Audiencia) => this.applyFilter(item, filter));
  }
  
  applyFilter(audiencia: Audiencia, filter: Audiencia): boolean {
    for (let field in filter) {
      if (filter[field]) {
        if (typeof filter[field] === 'string') {
          if (audiencia[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
            return false;
          }
        } else if (typeof filter[field] === 'number') {
          if (audiencia[field] !== filter[field]) {
            return false;
          }
        }
      }
    }
    return true;
  }
}