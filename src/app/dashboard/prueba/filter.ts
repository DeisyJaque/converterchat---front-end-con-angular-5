/**
 * Book entity, used for filtering as well.
 */
export class Audiencia {
  /**
   * @type {number} id Unique numeric identifier.
   */
  name: String;

  /**
   * @type {string} title The title of the book.
   */
  genero: String;

  /**
   * @type {string} author The author of the book.
   */
  status: String;

  /**
   * @type {number} year The year the book was published.
   */
  suscripcion: String;

   constructor() {
        }
}