// import { Component, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Component, Input, EventEmitter, Output, OnInit } from '@angular/core';
import { PaginatorService } from '../../services/paginator.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map'
import * as _ from 'underscore';

@Component({
  selector: 'app-audiencia',
  templateUrl: './audiencia.component.html',
  styleUrls: ['./audiencia.component.css']
})
export class AudienciaComponent implements OnInit {
  pager: any = {};

    // paged items
    pagedItems: any[];
    	allItems: any[];
 constructor(private http: HttpClient, private pagerService: PaginatorService) { }

    // array of all items to be paged


    // pager object



  ngOnInit() {

            console.log('################INIT######################');
	this.http.get('/subscriber/allfanpage/'+JSON.parse(sessionStorage.getItem('selectedpage'))['id'])
		.toPromise()
		.then(
			response => {

					this.allItems = response['Items'];
					this.setPage(1);
			},
			err => {
				console.error(`Error: ${err}`);
		});

  }
 setPage(page: number) {
        if (page < 1 || page > this.pager.totalPages) {
            return;
        }

        // get pager object from service
        this.pager = this.pagerService.getPager(this.allItems.length, page);

        // get current page of items
        this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
    }
}
