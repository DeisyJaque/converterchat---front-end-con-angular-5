import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Component, Input, EventEmitter, Output, OnInit , ViewContainerRef } from '@angular/core';
import { Pipe, PipeTransform } from '@angular/core';
import { FilterPipe } from '../prueba/filter.pipe';
import { PaginatorService } from '../../services/paginator.service';
// import {NgbModule,NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FanpageService } from  '../../services/fanpage.service';
import { Observable } from 'rxjs/Observable';
import { Audiencia } from '../prueba/filter';
import { NgForm, ReactiveFormsModule, FormsModule, FormBuilder, Validators,FormControl,FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import {Subject} from 'rxjs/Subject';
import {debounceTime} from 'rxjs/operator/debounceTime';
import {NgbAlertConfig} from './alert-config';
import 'rxjs/add/operator/map'
import * as _ from 'underscore';
//import * as $ from 'jquery';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';

@Component({
	selector: 'app-radiofusion',
	templateUrl: './radiofusion.component.html',
	styleUrls: ['./radiofusion.component.css']
})
export class RadiofusionComponent implements OnInit {

	public transmissions = [];
	public text: string;
	public title: string;
	allItems: any[];
	pager: any = {};
	pagedItems: any[];
//	audiencia:Audiencia[];
	len: number;
	limit: number;
	searchText: Audiencia = new Audiencia();
	listUsers: any[] = [];
	active = 'aud';
	sending = false;
	alerts = false;
	constructor(private http: HttpClient, private pagerService: PaginatorService, private router: Router, private userService: FanpageService , public toastr: ToastsManager , private vcr: ViewContainerRef) {
		 this.toastr.setRootViewContainerRef(vcr);
	}
	 private _success = new Subject<string>();
  closeResult: string;
  staticAlertClosed = false;
  successMessage: string;
	ngOnInit() {
	// get dummy data
	console.log('################INIT######################');
	this.http.get('/subscriber/allfanpage/'+JSON.parse(sessionStorage.getItem('selectedpage'))['id'])
		.toPromise()
		.then(
			response => {

					this.allItems = response['Items'];
					this.setPage(1);
			},
			err => {
				console.error(`Error: ${err}`);
		});


		this.http.get('/transmission/allfanpage/'+JSON.parse(sessionStorage.getItem('selectedpage'))['id'])
		.toPromise()
		.then(
			response => {
				this.transmissions = response['Items'];
				console.log(this.transmissions);
			},
			error => {console.log(error);}
		);
		setTimeout(() => this.staticAlertClosed = true, 20000);

    this._success.subscribe((message) => this.successMessage = message);
    debounceTime.call(this._success, 5000).subscribe(() => this.successMessage = null);

	}

	setPage(page: number) {

		console.log('setPage');
		if (page < 1 || page > this.pager.totalPages) {
			return;
		}

		// get pager object from service
		this.pager = this.pagerService.getPager(this.allItems.length, page);
		console.log(this.pager);
		// get current page of items
		this.pagedItems = this.allItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
		console.log(this.pagedItems);

	}

	user = new Audiencia();

	onSubmit(form: NgForm) {


		form.resetForm();
	}

 unir(id){
		console.log(id);
		let index;
		if(this.listUsers.length > 0){
			for(let l  in this.listUsers){
					if(this.listUsers[l] === id){
						console.log('igual');
						this.listUsers.splice(index , 1);
						// console.log(this.listUsers);
					}else{
						this.listUsers.push(id);
					}
			}
		}else{
			this.listUsers.push(id);
		}

		console.log(this.listUsers);
 }
// open(content:string) {
//   if ( content="send") {
// 		  this.toastr.success('El mensaje fue enviado exitosamente!', 'Success!');
//   	 this._success.next(`El mensaje fue enviado exitosamente!`);
//   }else{
//   	this.toastr.error('Hay un problema con el envio!', 'Oops!');
//   	this._success.next(` Hay un problema con el envio!`);
//   }
//   }

//   private getDismissReason(reason: any): string {
//     if (reason === ModalDismissReasons.ESC) {
//       return 'by pressing ESC';
//     } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
//       return 'by clicking on a backdrop';
//     } else {
//       return  `with: ${reason}`;
//     }
//   }
	audiencia(){
		console.log('audiencia')
		this.active = 'aud';
	}
	estadisticas(){
		console.log('estadisticas')
		this.active = 'est';
	}
	showSend(){
		this.sending = true;
	}
	cancelSend(){
		this.sending = false;
	}
	message(){
		console.log('Enviar');
		console.log(this.title);
		console.log(this.text);
		if(this.title != undefined && this.text != undefined ){
		    console.log('ejec envio');
			this.http.post('/transmission/',
			{
				pageAccessToken: JSON.parse(sessionStorage.getItem('selectedpage'))['access_token'],
				pageId: JSON.parse(sessionStorage.getItem('selectedpage'))['id'],
				recipients: this.listUsers,
				messageText: this.text,
				title: this.title
			})
				.toPromise()
				.then(
					response => { console.log(response);
						this.toastr.success('El mensaje fue enviado exitosamente!', 'Success!');
						this.title = "";
						this.text = "";
						this.listUsers = [];
						this.sending = false;
						this.alerts = true;
					},

					error => {console.log(error);
						this.toastr.error('Hay un problema con el envio!', 'Oops!');
					}
				);
		}else{
				this.toastr.error('Rellena todo los campos.', 'Oops!');
		}
	}
}
