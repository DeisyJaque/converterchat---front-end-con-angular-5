import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RadiofusionComponent } from './radiofusion.component';

describe('RadiofusionComponent', () => {
  let component: RadiofusionComponent;
  let fixture: ComponentFixture<RadiofusionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RadiofusionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RadiofusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
