import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { AuthService, SocialUser } from "angular5-social-login";
import { ApiService } from '../facebook/api.service';

@Injectable()
export class SigninGuard implements CanActivate {

	public user: SocialUser;

	constructor( private socialAuthService: AuthService, private router: Router, private social: ApiService) { }

	canActivate(): Observable<boolean> | Promise<boolean> | boolean {

		console.log("isLogin");
		if(localStorage.getItem('user') != null){
			return true;
		}

		this.router.navigate(['singin']);
		return false;
	}
}
