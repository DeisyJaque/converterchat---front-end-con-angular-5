import { TestBed, inject } from '@angular/core/testing';

import { FanpageService } from './fanpage.service';

describe('FanpageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [FanpageService]
    });
  });

  it('should be created', inject([FanpageService], (service: FanpageService) => {
    expect(service).toBeTruthy();
  }));
});
