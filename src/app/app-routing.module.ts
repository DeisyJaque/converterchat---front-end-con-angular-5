import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SigninGuard } from './guards/signin.guard';

/* COMPONENTS FOR DE ROUTER */
import { SigninComponent } from './signin/signin.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { SigninlistComponent } from './signinlist/signinlist.component';
import { LandingComponent } from './landing/landing.component';
import { PreciosComponent } from './precios/precios.component';
import { AudienciaComponent } from './dashboard/audiencia/audiencia.component';
import { PruebaComponent } from './dashboard/prueba/prueba.component';
import { RadiofusionComponent } from './dashboard/radiofusion/radiofusion.component';
import { TestingComponent } from './testing/testing.component';

const routes: Routes = [
	{
		path: '',
		component: LandingComponent
	},
	{
		path: 'dashboard',
		component: DashboardComponent,
		canActivate: [SigninGuard]
	},
	{
		path: 'dashboard/:id',
		component: DashboardComponent,
		canActivate: [SigninGuard]
	},
	{
		path: 'menu',
		component: MenuComponent,
		canActivate: [SigninGuard]
	},
	{
		path: 'singinlist',
		component: SigninlistComponent,
		//canActivate: [SigninGuard]
	},
	{
		path: 'singin',
		component: SigninComponent
	},
	{
		path: 'precios',
		component: PreciosComponent
	},
	{
		path: 'prueba',
		component: AudienciaComponent
	},
	{
		path: 'audiencia',
		component: PruebaComponent,
		canActivate: [SigninGuard]
	},
	{
		path: 'test',
		component: TestingComponent
	},
		{
		path: 'radiofusion',
		component: RadiofusionComponent,
		canActivate: [SigninGuard]
	}
];

@NgModule({
	imports: [RouterModule.forRoot(routes)],
	exports: [RouterModule],
	providers: [SigninGuard],
})
export class AppRoutingModule { }
