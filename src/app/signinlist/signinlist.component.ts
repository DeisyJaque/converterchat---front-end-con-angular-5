import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ApiService } from '../facebook/api.service';
import { SocialUser, LoginProviderClass } from '../facebook/entities/user';
import 'rxjs/add/operator/toPromise';

@Component({
	selector: 'app-signinlist',
	templateUrl: './signinlist.component.html',
	styleUrls: ['./signinlist.component.css']
})
export class SigninlistComponent implements OnInit {

	public user: SocialUser;
	public loggedIn: boolean;
	public pages = [];
	public selectedValue = null;

	constructor( private router: Router, private http: HttpClient, private social: ApiService) { }

	ngOnInit() {

		this.http.get('/api/accounts/'+JSON.parse(localStorage.getItem('user'))['token'])
			.toPromise()
			.then(
				response => {
					console.log('get',response);
					if(response['data'] != null){
						this.pages = response['data'];
						localStorage.setItem('pages', JSON.stringify(this.pages));
					}else{
						localStorage.removeItem('user');
						this.router.navigate(['singin']);
					}
				},
				msg => {
					console.error(`Error: ${msg.status} ${msg.statusText}`);
					localStorage.removeItem('user');
					this.router.navigate(['singin']);
			});

	}


	onSubmit(regForm:NgForm) {

		let page =  this.pages.find(x => x.id == this.selectedValue);
		page.userId = JSON.parse(localStorage.getItem('user'))['id'];

		console.log(page);
		sessionStorage.setItem('selectedpage', JSON.stringify(page));

		this.http.post('/api/subscribepage/', page)
			.subscribe(res => {
				this.router.navigate(['dashboard',page.id]);
			}, (err) => {
				console.log('>>>>>>>>>>onSubmit>>>>>>>>>>>>>>>>>>>>');
				console.log(err);
			});
	}

}
