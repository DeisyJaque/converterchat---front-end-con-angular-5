import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';


@Component({
	selector: 'app-testing',
	templateUrl: './testing.component.html',
	styleUrls: ['./testing.component.css']
})
export class TestingComponent implements OnInit {

	public transmissions = [];

	constructor(private router: Router, private http: HttpClient) { }

	ngOnInit() {

		this.http.get('/transmission/allfanpage/'+JSON.parse(sessionStorage.getItem('selectedpage'))['id'])
		.toPromise()
		.then(
			response => {
				this.transmissions = response['Items'];
				console.log(this.transmissions);
			},
			error => {console.log(error);}
		);
	}

	onSubmit(f: NgForm) {
		console.log(f.value);  // { first: '', last: '' }
		console.log(f.valid);  // false

			this.http.post('/api/test/',f.value)
			.toPromise()
			.then(
				response => { console.log(response);},
				error => {console.log(error);}
			);
	}

}
