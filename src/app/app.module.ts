import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

/* MODULE HTTP FOR SERVER COMUNICATION */
import { HttpClientModule } from '@angular/common/http';
/* FORMS MODULE */
import { FormsModule,ReactiveFormsModule }   from '@angular/forms';

/* FACEBOOK DE API PARA ANGULAR */
import { SocialLoginModule, AuthServiceConfig, FacebookLoginProvider} from "angular5-social-login";
import { ApiService } from './facebook/api.service';

/* ROUTING MODULE */
import { AppRoutingModule } from './app-routing.module';

/* GUARDS FOR ROUTING */
import { SigninGuard } from './guards/signin.guard';

/* COMPONENTS */
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { MenuComponent } from './menu/menu.component';
import { SigninComponent } from './signin/signin.component';
import { SigninlistComponent } from './signinlist/signinlist.component';
import { ToolsComponent } from './tools/tools.component';
import { LandingComponent } from './landing/landing.component';
import { PreciosComponent } from './precios/precios.component';
import { AudienciaComponent } from './dashboard/audiencia/audiencia.component';
import { PaginatorService } from './services/paginator.service';
import { FanpageService } from './services/fanpage.service';
import { PruebaComponent } from './dashboard/prueba/prueba.component';
import { FilterPipe } from './dashboard/prueba/filter.pipe';
import { RadiofusionComponent } from './dashboard/radiofusion/radiofusion.component';
import { TestingComponent } from './testing/testing.component';
import { NavComponent } from './nav/nav.component';
import { MyDatePickerModule } from 'mydatepicker';
import {ToastModule} from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ChartsModule } from 'ng2-charts';
import { OrderByPipe } from './pipes/order-by.pipe';

/* EXPORT FACEBOOK CONFIG */
export function getAuthServiceConfigs() {
	let config = new AuthServiceConfig([
		{
			id: FacebookLoginProvider.PROVIDER_ID,
			provider: new FacebookLoginProvider("1941120542772303")
		}
	]);
	return config;
}

@NgModule({
	declarations: [
		AppComponent,
		DashboardComponent,
		MenuComponent,
		SigninComponent,
		SigninlistComponent,
		ToolsComponent,
		LandingComponent,
		PreciosComponent,
		AudienciaComponent,
		PruebaComponent,
		FilterPipe,
		RadiofusionComponent,
		TestingComponent,
		NavComponent,
		OrderByPipe
	],
	imports: [
		BrowserModule,
		BrowserAnimationsModule,
		FormsModule,
		ReactiveFormsModule,
		AppRoutingModule,
		HttpClientModule,
		SocialLoginModule,
		MyDatePickerModule,
		ToastModule.forRoot(),
		ChartsModule
	],
	providers: [
		{
			provide: AuthServiceConfig,
			useFactory: getAuthServiceConfigs
		},
		SigninGuard,
		ApiService,
		PaginatorService,
		FanpageService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
