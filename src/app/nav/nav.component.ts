import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { ApiService } from '../facebook/api.service';
import { SocialUser, LoginProviderClass } from '../facebook/entities/user';
import 'rxjs/add/operator/toPromise';
//import * as $ from 'jquery';

@Component({
	selector: 'app-nav',
	templateUrl: './nav.component.html',
	styleUrls: ['./nav.component.css']

})
export class NavComponent implements OnInit {

	constructor(private router: Router, private http: HttpClient, private social: ApiService) { }

	ngOnInit() {
	}

	signOut(){
		console.log('#############################');
		localStorage.removeItem('user');
		alert("singOut component");
		this.router.navigate(['singin']);
	}
	menu(){
		console.log('Menu');
		// $('body').toggleClass( "mini-navbar" );
	}
}
